package com.person.kalkulatiori

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var firstNumber:Double = 0.0
    private var secondNumber:Double = 0.0
    private var operation = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        button0.setOnClickListener {
            calc(button0)
        }
        button1.setOnClickListener {
            calc(button1)
        }
        button2.setOnClickListener {
            calc(button2)
        }
        button3.setOnClickListener {
            calc(button3)
        }
        button4.setOnClickListener {
            calc(button4)
        }
        button5.setOnClickListener {
            calc(button5)
        }
        button6.setOnClickListener {
            calc(button6)
        }
        button7.setOnClickListener {
            calc(button7)
        }
        button8.setOnClickListener {
            calc(button8)
        }
        button9.setOnClickListener {
            calc(button9)
        }
        buttonPlus.setOnClickListener {
            addOperation(buttonPlus)
        }
        buttonMinus.setOnClickListener {
            addOperation(buttonMinus)
        }
        buttonGay.setOnClickListener {
            addOperation(buttonGay)
        }
        buttonGamr.setOnClickListener {
            addOperation(buttonGamr)
        }
        buttonEqual.setOnClickListener {
            equal()
        }
        buttonDel.setOnClickListener {
            delete()
        }
        buttonDel.setOnLongClickListener {
            buttondisplay.text = ""
            firstNumber = 0.0
            secondNumber = 0.0
            true

        }
        buttonDot.setOnClickListener{
            calc(buttonDot)
        }

    }

    private fun calc(button: Button) {
        buttondisplay.text = buttondisplay.text.toString() + button.text

    }


    private fun delete() {
        var text = buttondisplay.text.toString()
        if (text.isNotEmpty()) {
            text = text.substring(0, text.length - 1)
            buttondisplay.text = text


        }

    }


    private fun addOperation(button: Button) {
        firstNumber = buttondisplay.text.toString().toDouble()
        operation = button.text.toString()
        buttondisplay.text = ""
    }

    private fun equal() {
        if (buttondisplay.text.isNotEmpty()) {
            secondNumber = buttondisplay.text.toString().toDouble()
            var result: Double = 0.0
            if (operation == "+") {
                result = firstNumber + secondNumber
            } else if (operation == "-") {
                result = firstNumber - secondNumber
            } else if (operation == "*") {
                result = firstNumber * secondNumber
            } else if (operation == ":") {
                result = firstNumber / secondNumber
            }
            buttondisplay.text = result.toString()
            firstNumber = 0.0
        }
    }

}